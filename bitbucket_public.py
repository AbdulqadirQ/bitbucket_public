def multiply_by_10(num):
    return 10 * num

print(multiply_by_10(4))

class Dogs:
    def __init__(self):
        self._num_of_legs = 8

    def print_legs(self):
        print(self._num_of_legs)
